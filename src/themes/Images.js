import background from '../assets/img/background.jpg';
import appLogo from '../assets/img/appLogo.png';
import defaultUser from '../assets/img/default.png';

const Images = {
  background,
  appLogo,
  defaultUser,
};

export default Images;
