const colors = {
  default: '#ffffff',
  background: '#f0f0f0',
  // tabsStyle
  tabSelected: '#FF9800',
  tabBackground: '#252C33',

  divider: '#d1d1d1',
  lightDivider: '#F4F3F3',
  blur: 'rgba(0, 0, 0, 0.3)',
  blur0: 'rgba(100, 100, 100, 0.2)',
  blur1: 'rgba(0, 0, 0, 0.6)',
  superDarkBlur: 'rgba(0, 0, 0, 0.7)',
  navigator: '#F7F6F2',
  iconNav: '#3f4e5a',
  whiteSmoke: '#f4f4f4',
  //
  primary: '#FF9800',
  secondary: '#0097a7',
  lightprimary: '#F7962C',
  darkprimary: '#F26122',
  // text color
  primaryText: '#000',
  primaryTextBlur: '#808080',
  secondaryText: '#8E8D94',
  placeholderText: '#1D1D2650',

  backgroundNav: '#0097a7',
  backgroundNavGradient: '#c9caca',
  titleNav: '#fff',
  disabledNavButons: '#bdbdbd',

  // global color
  red: '#F1403C',
  lightBlue: '#00F0FF',
  blue: '#00adee',
  gray: '#858585',
  lightGray: '#9B9B9B',
  secondaryGray: '#eff0f0',
  thirdGray: '#e5e4e4', // for close button background
  darkGray: '#444444',
  grayBlue: '#747474',
  light: '#f5f5f5',
  yellow: '#cea849',
  heart: '#ff2d3b',
  orange: '#f17c30',
  green: '#35b050',

  facebook: '#2F4779',
  google: '#F24033',
  starColors: ['#CD1C26', '#FF7800', '#CDD614', '#5BA829', '#3F7E00'],
};

export default colors;
