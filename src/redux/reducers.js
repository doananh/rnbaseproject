import login from './LoginRedux/reducer';
import tutor from './TutorRedux/reducer';

export default {
  login,
  tutor,
};
