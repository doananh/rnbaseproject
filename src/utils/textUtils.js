import moment from 'moment';

export const formatUnixToDate = unit => moment.unix(unit).format();

export const upperFirstChar = (text) => {
  return text.replace(/\w\S*/g, (txt) => {
    return txt.charAt(0).toUpperCase() + txt.substr(1);
  });
};
export const lowerFirstChar = (text) => {
  return text.charAt(0).toLowerCase() + text.substr(1);
};
export const replaceAll = (text, search, replacement) => {
  return text.replace(new RegExp(search, 'g'), replacement);
};

export const makeActionName = (text) => {
  return lowerFirstChar(
    replaceAll(upperFirstChar(replaceAll(text, '_', ' ').toLowerCase()), ' ', ''),
  );
};
